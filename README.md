![logo](https://gitlab.com/CurveML/CurveML/-/raw/main/pics/curveml-logo.png)

# CurveML Dataset

This is the official repository of the paper: _"CurveML: a Benchmark for evaluating and training learning-based methods of Classification, Recognition, and Fitting of Plane Curves"_

ResNet-101 Image Classification problem
----------------------------

[Jupyter Notebook - classification](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-classification-dataset-v1.0-wo-splines.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-classification-dataset-v1.0-wo-splines.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/cls/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-28_18.34.03-BS-32-LR-0.0005-0.001-best.pth) )

ResNet-101 Image Regression problems
-------------------------

[Jupyter Notebook - npetals](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-npetals.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-npetals.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/npetals/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-23_18.50.16-BS-32-LR-0.0005-0.0010446481173858047-best.pth) )

[Jupyter Notebook - a](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-a.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-a.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/a/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-24_12.59.41-BS-32-LR-0.0005-0.001-best.pth) )

[Jupyter Notebook - b](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-b.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-b.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/b/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-28_19.11.33-BS-32-LR-0.001-0.003-best.pth) )

[Jupyter Notebook - angle](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-angle.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-angle.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/angle/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-25_09.00.04-BS-32-LR-0.001096478197723627-0.0054823909886181355-best.pth) )

[Jupyter Notebook - x](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-x.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-x.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/x/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-26_22.33.38-BS-32-LR-0.0005-0.001-best.pth) )

[Jupyter Notebook - y](https://htmlpreview.github.io/?https://gitlab.com/CurveML/CurveML/-/raw/main/html/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-y.html?inline=true) ( [source](https://gitlab.com/CurveML/CurveML/-/raw/main/notebooks/geometric-primitives-classification-v1.2-regression-dataset-v1.0-wo-splines-y.ipynb) - [pretrained model](https://gitlab.com/CurveML/CurveML/-/raw/main/models/y/geometric-primitives-dataset-v1.0-wo-splines-resnet101--no-data-aug-img_size-250-250-1a-2023-03-28_16.10.49-BS-32-LR-0.0005-0.001-best.pth) )


PointNet Evaluation Notebooks
-------------------------

[Classification](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-classification-evaluation-dataset-v1.0-wo-splines.html?inline=true)

[Regression - angle](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-angle.html?inline=true)

[Regression - npetals](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-n_petals.html?inline=true)


[Regression - a](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-a.html?inline=true)

[Regression - b](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-b.html?inline=true)


[Regression - x](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-trans_x.html?inline=true)

[Regression - y](https://htmlpreview.github.io/?https://gitlab.com/CurveML/curveml-pointnet/-/raw/main/notebooks/html/geometric-primitives-classification-v1.2-pointnet-regression-evaluation-dataset-v1.0-wo-splines-trans_y.html?inline=true)

